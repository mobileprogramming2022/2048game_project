import 'dart:io';
import 'dart:math';
import 'BoardGame.dart';
import 'mainPlayer.dart';

class Game {
  late mainPlayer player;
  late BoardGame board;

  Game() {
    player = new mainPlayer("");
  }
  void newBoard() {
    board = new BoardGame(player);
  }

  void showWelcomeGame() {
    print("██████╗░░█████╗░░░██╗██╗░█████╗░");
    print("╚════██╗██╔══██╗░██╔╝██║██╔══██╗");
    print("░░███╔═╝██║░░██║██╔╝░██║╚█████╔╝");
    print("██╔══╝░░██║░░██║███████║██╔══██╗");
    print("███████╗╚█████╔╝╚════██║╚█████╔╝");
    print("╚══════╝░╚════╝░░░░░░╚═╝░╚════╝░");
    print("How to play");
    print(
        "1. The game will have a 4x4 table size for players. And will prepare numbers for the players at least 2 numbers in the table");
    print(
        "2. Players have to press w,a,s,d to move the numbers. to bring the numbers together");
    print(
        "3. When the player press w,a,s,d to move and then New numbers will be randomly drawn in the table");
    print(
        "4. Numbers are randomly generated each time the player presses a letter in one scroll");
    print(
        "5. The number are randomly generated until the numbers in the the grid are filled. and when the numbers in the table are full will be considered GameOver");
    print(
        "6. But if the player advances all the numbers to 2048, the player is win");
    print("Good Luck :)");
    print("---------------------------------");
  }

  void showTableGame() {
    var table = board.getTable();
    for (int i = 0; i < table.length; i++) {
      for (int j = 0; j < table[i].length; j++) {
        stdout.write(table[i][j] + " ");
      }
      print("");
    }
    print("-----------------------");
  }

  void randomNum() {
    board.setBoard();
  }

  void inputDirectionSymbol() {
    print("input the direction: ");
    String? direction = stdin.readLineSync();
    if (board.Process(direction!.toUpperCase())) {
      board.setBoard();
    }
  }

  void showScore() {
    var table = board.getTable();
    var scoreCount = 0;
    for (int row = 0; row < table.length; row++) {
      for (int col = 0; col < table[row].length; col++) {
        if (table[row][col] != "-") {
          if (int.parse(table[row][col]) > scoreCount) {
            scoreCount = int.parse(table[row][col]);
          }
        }
      }
    }
    stdout.write("Your Score: " + scoreCount.toString() + "\n");
  }

  bool checkGameOver() {
    if (board.checkGameOverW() ||
        board.checkGameOverS() ||
        board.checkGameOverA() ||
        board.checkGameOverD()) {
      return false;
    }
    return true;
  }

  showGameOver() {
    print("GameOver!!!");
    print("You want to play?, Please enter to play");
    String? x = stdin.readLineSync();
    print("Let's Play~");
  }

  showGameWin() {
    print("You Win!!!");
    String? x = stdin.readLineSync();
  }

  clearScreen() { // เคลียร์หน้าจอ
    print("\x1B[2J\x1B[0;0H");
  }

  bool checkWin() {
    var table = board.getTable();
    for (int row = 0; row < table.length; row++) {
      for (int col = 0; col < table[row].length; col++) {
        if (table[row][col] == "2048") {
          return true;
        }
      }
    }
    return false;
  }
}
