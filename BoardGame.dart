import 'dart:ffi';
import 'dart:io';
import 'dart:math';
import 'mainPlayer.dart';

class BoardGame {
  var table = [
    ['-', '-', '-', '-'],
    ['-', '-', '-', '-'],
    ['-', '-', '-', '-'],
    ['-', '-', '-', '-']
  ];
  late mainPlayer isPlayer;
  late mainPlayer player;
  var scoreCount = 0;
  bool win = false;
  bool gameOver = false;

  BoardGame(mainPlayer player) {
    player = player;
    isPlayer = player;
    scoreCount = 0;
  }

  List<List<String>> getTable() {
    return table;
  }

  mainPlayer getIsPlayer() {
    return isPlayer;
  }

  mainPlayer getPlayer() {
    return player;
  }

  int getScoreCount() {
    return scoreCount;
  }

  bool youWin() {
    return win;
  }

  bool getGameOver() {
    return gameOver;
  }

  void setBoard() {
    int col;
    int row;
    do {
      col = Random().nextInt(4);
      row = Random().nextInt(4);
    } while (table[col][row] != '-' && checkRD());
    if (checkRD()) {
      table[col][row] = "" + (Random().nextInt(1) + 2).toString();
    }
  }

  bool checkRD() {
    for (int row = 0; row < table.length; row++) {
      for (int col = 0; col < table[row].length; col++) {
        if (table[row][col] == '-') {
          return true;
        }
      }
    }
    return false;
  }

  bool Process(String direction) {
    switch (direction) {
      case 'W':
        {
          checkW();
          return true;
        }

      case 'A':
        {
          checkA();
          return true;
        }

      case 'S':
        {
          checkS();
          return true;
        }
      case 'D':
        {
          checkD();
          return true;
        }
      default:
        {
          print("error!!!");
          return false;
        }
    }
  }

  void checkW() {
    for (int row = 0; row < table.length; row++) {
      for (int col = 0; col < table[row].length; col++) {
        if (table[row][col] != '-') {
          var a1 = table[row][col];
          for (int i = row - 1; i > -1; i--) {
            var x1 = table[i][col];
            if (x1[0] == '.') {
              break;
            }
            if (table[i][col] != '-') {
              if (a1 == table[i][col]) {
                table[i][col] = "." + (int.parse(a1) * 2).toString();
                table[row][col] = '-';
                table[i + 1][col] = '-';
                break;
              } else {
                break;
              }
            } else {
              table[i][col] = a1;
              table[i + 1][col] = '-';
            }
          }
        }
      }
    }
    reset();
  }

  void checkS() {
    //เช็คทิศทางในการเลื่อนไปทิศ s
    for (int row = table.length - 1; row > -1; row--) {
      for (int col = 0; col < table[row].length; col++) {
        if (table[row][col] != '-') {
          var a1 = table[row][col];
          for (int i = row + 1; i < 4; i++) {
            var x1 = table[i][col];
            if (x1[0] == '.') {
              break;
            }
            if (table[i][col] != '-') {
              if (a1 == table[i][col]) {
                table[i][col] = "." + (int.parse(a1) * 2).toString();
                table[row][col] = '-';
                table[i - 1][col] = '-';
                break;
              } else {
                break;
              }
            } else {
              table[i][col] = a1;
              table[i - 1][col] = '-';
            }
          }
        }
      }
    }
    reset();
  }

  void checkD() {
    for (int col = table.length - 1; col > -1; col--) {
      for (int row = 0; row < table[col].length; row++) {
        if (table[row][col] != '-') {
          var a1 = table[row][col];
          for (int i = col + 1; i < 4; i++) {
            var x1 = table[row][i];
            if (x1[0] == '.') {
              break;
            }
            if (table[row][i] != '-') {
              if (a1 == table[row][i]) {
                table[row][i] = "." + (int.parse(a1) * 2).toString();
                table[row][col] = '-';
                table[row][i - 1] = '-';
                break;
              } else {
                break;
              }
            } else {
              table[row][i] = a1;
              table[row][i - 1] = '-';
            }
          }
        }
      }
    }
    reset();
  }

  void checkA() {
    for (int col = 0; col < table.length; col++) {
      for (int row = 0; row < table[col].length; row++) {
        if (table[row][col] != '-') {
          var a1 = table[row][col];
          for (int i = col - 1; i > -1; i--) {
            var x1 = table[row][i];
            if (x1[0] == '.') {
              break;
            }
            if (table[row][i] != '-') {
              if (a1 == table[row][i]) {
                table[row][i] = "." + (int.parse(a1) * 2).toString();
                table[row][col] = '-';
                table[row][i + 1] = '-';
                break;
              } else {
                break;
              }
            } else {
              table[row][i] = a1;
              table[row][i + 1] = '-';
            }
          }
        }
      }
    }
    reset();
  }

  void reset() {
    for (int row = 0; row < table.length; row++) {
      for (int col = 0; col < table[row].length; col++) {
        var x1 = table[row][col];
        if (x1[0] == '.') {
          table[row][col] = x1.substring(1, x1.length);
        }
      }
    }
  }

  bool checkGameOverW() {
    for (int row = 0; row < table.length; row++) {
      for (int col = 0; col < table[row].length; col++) {
        if (table[row][col] != '-') {
          var a1 = table[row][col];
          for (int i = row - 1; i > -1; i--) {
            var x1 = table[i][col];
            if (table[i][col] != '-') {
              if (a1 == table[i][col]) {
                return true;
              } else {
                break;
              }
            } else {
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  bool checkGameOverS() {
    for (int row = table.length - 1; row > -1; row--) {
      for (int col = 0; col < table[row].length; col++) {
        if (table[row][col] != '-') {
          var a1 = table[row][col];
          for (int i = row + 1; i < 4; i++) {
            var x1 = table[i][col];
            if (table[i][col] != '-') {
              if (a1 == table[i][col]) {
                return true;
              } else {
                break;
              }
            } else {
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  bool checkGameOverD() {
    for (int col = table.length - 1; col > -1; col--) {
      for (int row = 0; row < table[col].length; row++) {
        if (table[row][col] != '-') {
          var a1 = table[row][col];
          for (int i = col + 1; i < 4; i++) {
            var x1 = table[row][i];
            if (table[row][i] != '-') {
              if (a1 == table[row][i]) {
                return true;
              } else {
                break;
              }
            } else {
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  bool checkGameOverA() {
    for (int col = 0; col < table.length; col++) {
      for (int row = 0; row < table[col].length; row++) {
        if (table[row][col] != '-') {
          var a1 = table[row][col];
          for (int i = col - 1; i > -1; i--) {
            var x1 = table[row][i];
            if (table[row][i] != '-') {
              if (a1 == table[row][i]) {
                return true;
              } else {
                break;
              }
            } else {
              return true;
            }
          }
        }
      }
    }
    return false;
  }
}
