import 'dart:io';

import 'BoardGame.dart';

class mainPlayer {
  var symbolPlayer = " ";
  var score = 0;
  var win = 0;
  var gameOver = 0;

  mainPlayer(String symbolPlayer) {
    this.symbolPlayer = symbolPlayer;
  }
  String getSymbolPlayer() {
    return symbolPlayer;
  }

  void setSymbolPlayer(String symbolPlayer) {
    this.symbolPlayer = symbolPlayer;
  }

  int getScore() {
    return score;
  }

  void setScore() {
    this.score++;
  }

  int getWin() {
    return win;
  }

  void setWin() {
    this.win++;
  }

  int getGameOver() {
    return gameOver;
  }

  void setGameOver() {
    this.gameOver++;
  }
}
