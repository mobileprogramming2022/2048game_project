import 'dart:io';

import 'Game.dart';

void main(List<String> arguments) {
  Game game = new Game();
  game.showWelcomeGame();
  game.newBoard();
  game.randomNum();
  while (true) {
    game.showTableGame();
    game.showScore();
    game.inputDirectionSymbol();
    game.clearScreen();
    if (game.checkWin()) {
      game.showTableGame();
      game.showScore();
      game.showGameWin();
      game.newBoard();
      game.randomNum();
    } else if (game.checkGameOver()) {
      game.showTableGame();
      game.showScore();
      game.showGameOver();
      game.newBoard();
      game.randomNum();
    }
  }
}
